import React,{useState} from 'react'
import ChatBox from './ChatBox'
import './Widget.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faComments, faTimes } from '@fortawesome/free-solid-svg-icons'
import './Home.css'
import axios from 'axios'
import Availability from './Available'
import socketMain from "./socketconfig";


function Home() {
    const baseURL= "http://1385458cfa4e.ngrok.io"
    const [availability,setAvailability] = useState(false)
    const [toggle,setToggle] = useState(true)
    const [start, setStart] = useState(true)
    const [agent_id, setAgentid] = useState();
    const [conversation_id, setId] = useState();
    
    
    var username = "PARENT_SERVER";
    var password = "PAR123783PASS";
    let headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization:
        "Basic " + new Buffer.from(username + ":" + password).toString("base64"),
    };
    const createConversation =async ()=>{
        const channelType = "WEB";
        const ticketId = 154226;
        const catagoryID = 6253372;
        const subCatagoryID = 2633735236;
        await axios
          .post(
            `${baseURL}/createconversation`,
            {
              accountName: "TEST",
              userID: "ofvoifidwdcvdhvgdththtthvhfuyfewddoih",
              additionalInfo: {
                channelType: channelType,
                ticketID: ticketId,
                catagoryID: catagoryID,
                subCatagoryID: subCatagoryID,
              },
            },
            {
              headers: headers,
            }
          )
          .then((res) => {
            const result = res.data;
            console.log(result);
            setId(result.conversationID);
            setAgentid(result.agentID);
            console.log(result.agentID);
            console.log("Inside start");
             
            console.log(result.agentid)
            setStart(false);
            socketMain.emit("clientRegister", {
              clientid: result.clientID,
              conversationid: result.conversationID,
              agentid: result.agentID,
            });
            
console.log("CLient registered")
            // console.log(res);
          })
          .catch((err) => {
            console.log(err);
          });
        console.log("conversation created");
    }

    const handleToggle =async() =>{
setToggle(false)

      var account = "TEST"
      const username = "PARENT_SERVER"
      const password = "PAR123783PASS"
      let headers = {
        "Accept":"application/json",
        "Content-Type": "application/json",
        'Authorization':'Basic ' + new Buffer.from(username + ':' + password).toString("base64")
      }
        console.log("Getting Agents Availability")
        !availability?await axios.get(`${baseURL}/client/${account}/availableagents`,{
headers:headers
        }).then((res)=>{
           console.log(res)
     
     if(res.data.availableagents>0){
        setAvailability(true)
     }
         }):

      setStart(false)

    createConversation()
}
    
    const closeChat=()=>{
setToggle(true)

    }
    const data ={
        conversation_id:conversation_id,
        agent_id:agent_id,
        accountName:"TEST"
    }
 
    return (
        <div >
         <div className="home">
            {availability&&!toggle?<ChatBox data={data} />:""}
           {!availability&&!toggle?<Availability/>:""}
           { toggle?<div className="  "><button  className={toggle?"home1 widget ":""} onClick={handleToggle}><FontAwesomeIcon className="icon" icon={faComments}/ ></button></div>:<button className="widget wid"  onClick={closeChat}><FontAwesomeIcon className="icon" icon={faTimes}/ ></button>}
           
        </div>
        </div>
    )
}

export default Home
