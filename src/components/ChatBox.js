import React from "react";
import { Button, Navbar, Form } from "react-bootstrap";
import { useState, useRef, useCallback } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle, faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import socketMain from "./socketconfig";

import "./chatBox.css";

const ChatBox = (props) => {
  console.log(props);

  // const Days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri"];
  var today = new Date();
  const baseURL = "http://1385458cfa4e.ngrok.io";
  // console.log(
  //   (today.getDate() < 10 ? "0" : "") +
  //     today.getDate() +
  //     "/" +
  //     (today.getMonth() + 1 < 10 ? "0" : "") +
  //     (today.getMonth() + 1) +
  //     "/" +
  //     today.getFullYear()
  // );
  var username = "PARENT_SERVER";
  var password = "PAR123783PASS";
  let headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization:
      "Basic " + new Buffer.from(username + ":" + password).toString("base64"),
  };

  // console.log();
  var hrs = today.getHours();
  var am = hrs < 12 ? "am" : "pm";
  // var time =
  //   (today.getHours() < 10 ? "0" : "") +
  //   today.getHours() +
  //   ":" +
  //   (today.getMinutes() < 10 ? "0" : "") +
  //   today.getMinutes() +
  //   am;
  // console.log(time);
  
  const [sentMessage, setsentMessage] = useState("");

  // const [internalConversation, setInternal] = useState([{}]);

  const [conversation, setConversation] = useState([]);
  console.log(props.data.conversation_id);

  // const lastMessageRef = useRef(null);
  const setRef = useCallback((node) => {
    if (node) {
      node.scrollIntoView({ smooth: true });
    }
  }, []);

  const messageRef = useRef();

  socketMain.on("clientMessage", (data) => {
    console.log(data);
    console.log(data.messageDetails);

    getConversation();
  });

  const getConversation =async () => {
    var conData;
    var accountname = "TEST";
// console.log(object)
const deatils = {
  agentid: props.data.agent_id,
  conversationid: props.data.conversation_id,
  accountname: accountname,
}
console.log(deatils)

   await axios
      .post(
        `${baseURL}/client/conversations/messages`,
        deatils
        ,
        {
          headers: headers,
        }
      )
      .then((data1) => {
        conData = data1.data.data;
        console.log(conData);
        setConversation(conData);
      });
  };

  socketMain.on("agentStatus", (data) => {
    console.log(data);
    const getStatus = () => {
      axios
        .get(`${baseURL}/client/TEST/6416/agentstatus`, {
          headers: headers,
        })
        .then((res) => {
          console.log(res);

          console.log("status is", res.data.data);
        });
    };
    getStatus();
  });

  const date =
    (today.getDate() < 10 ? "0" : "") +
    today.getDate() +
    "/" +
    (today.getMonth() + 1 < 10 ? "0" : "") +
    (today.getMonth() + 1) +
    "/" +
    today.getFullYear() +
    "   " +
    ((today.getHours() < 10 ? "0" : "") +
      today.getHours() +
      ":" +
      (today.getMinutes() < 10 ? "0" : "") +
      today.getMinutes()) +
    am;
  const updateConversation = async (e) => {
    e.preventDefault();
    if (messageRef.current.value.trim().length !== 0) {
      const message = {
        message: messageRef.current.value,
        createdAt: date,
        conversationid: props.data.conversation_id,
        accountname: "TEST",
      };
      const backUp = [...conversation, message];
      console.log(message);
      setConversation(backUp);
      await axios
        .post(
          `${baseURL}/sendClientMessage`,
          {
            message: messageRef.current.value,
            conversationid: props.data.conversation_id,
            accountname: "TEST",
          },
          {
            headers: headers,
          }
        )
        .then((data) => {
          console.log(data);
        });

      console.log(messageRef.current.value);

      

      // .then((res) => {
      //   console.log(res);
      //   console.log("inside send");
      //   messageRef.current.value=""

      // }).catch(()=>{
      //   messageRef.current.value=""

      //   console.log("Message Sent");

      // })

      // var today = new Date();
      // var time = today.getHours() + ":" + today.getMinutes();
    }
    messageRef.current.value = "";
    console.log(conversation);
    // setsentMessage("");
    console.log("new messages added", sentMessage);
  };

  return (
    <div className="maincontainer">
      <div className="header">
        <Navbar bg="light" variant="dark" className="header">
          <div className="heading">
            <img
              alt=""
              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAABAlBMVEX////wXCb1lB77/Pz//fzwXikHMEsAHDoAKUXz6BH//PwAHjzvUBYAIT7//vz1liJFZHhjfI4AJkMCLEjvVh7wWSJrg5Pa4OTJ0tgyVGp6kJ/0jhHR2d5AX3Tp7O8lSWEONVD3qIv5wq7xazr84dcAFjWzwMiPoa7+8+/708T2mHb0hVz2miv0iwzzek73pkT828/xZzT96eH97933pYf83bf4uKDydEb+9en6x4n3rFD6zZb4sl70imP16y/2oTr9+s8aQFmltL7716z958z48nr69qX48Wv5vHL69Zj5vaf+/en7+LeFmaf4uGr1k3D8+b/84L/27Un38F/584b6y5HCiTBBAAAMvklEQVR4nO1cCVvayhoeyEIgJEISDGugIItFrVvVLlaD3Y7Wettzzv//K/f7ZrJBorcFuUz65O1TncbJMO98+8xUQjJkyJAhQ4YMGTJkyJAhQ4YMGf4IiJuewDNB/DOYAInd401P4hkAPE7P/gCRiKQ0nW56Es8AkRyf5ybpF4hIDo/K/T+Bx65U/gMUC3hYOWs39QJhPKZ/Ao9aTUq/hYhkR6pJ1nnKaZASmQwsSSq/SLtACJmWpVyutpNyIiI5BR6SdbDpiayIEjgskIdUPoVmqoEGgkRSHUR0kMJLNBBgcpxiIic6IYcWpWGdp1axdPLxBIQwRcUCIqnNs3RycoMh3VOs8svUapb49QLmfmB5RNLqtHTylQqE8chhXE8lEZ28fktEkTyUQyLpVC197wewOWY00isRkXx/RXSRnHkCSauNgOfd/4GaNbUCIin1WldXKJaJVPNVK5XloU7e7d+gZu2WAxtJZ2S/2Nu6QIm8CIjQXCtt0Mmb/e/wVSSXIREpjVsoF3v7GAyJeG6FRNIXSFAgexfYmhzVQs1KYYV4sdd6S+heby6KtBUk4LJarTe0pNqpRXmU03aioF/lW6/xDGGBSMp0C7LF1lb+JE4kbX5LJ99beWrrYpgyerp1mSYiYOr51ivWLA3mRIJbdKkJ7yK5aW1Rp4U4sKS0ikQk74HIezph0d8JSqWVgGZttTBBQSL9RSKpcVzUZ4VE5q09TbFEJF+RyHt/uvNGgkjNlvyrVj7vG3uk1E2dcoGJAJFXbNUXs60cq91TIBKo1UEgeRYQCR4pXJZjZpKGk0QaRVAkH9lko8VuEBWlFJgJliJAZGv/HXVbiJi5S9aA/3scOoZDIBK4rdJiKGEGz/+tLf0VtZHASADnMQ9Mr3LwzeSOOi0Uyc0TIuGdiU6u7yZbHpG34USnMZHwzUQnd5/JCSNC/RYzd9E7e4sxKfHKRCef7gIiaO6e3yot5sAekwNOfZdOvn3yAjsTyY9gnpOjWgITa8DrrsrPa1Ckq5ZP5JX/PJbNcx1PdPL5A35/6xPZ2n/jK1d4brWgXXwWjB++4P7719ZWRLn88M4uP8SZ8FedgOstfiY6bs6FRPYuiJ8F7yZ4LoB1yNtuBLis4jVK5GM+wBZm874PfvGImXDGA5h8QCK4zdiKMHkbhr1pspk88KVcqFmUyJyRIBOdyaT0qJnwVZ3o5K9i8T8EZ/0jlAjVrgljAgE+0Upq0jFPZqKTL8XiX3TKUd1CJlcnfo2VHE14u9T8s1j8m9oIK60iTPI3xNuuSzZ4vnww2HqxeEdwuidRHsAk3/rqM0lKuugBED/KRYn8gyKZN3cW49/7ziu2FcGfciEREAn6rZP8AnwmpcTaBJjccqNcOvkbifxk7e8LImFMCL0KkeSEa0fcZI86+QeJFD9BlpIgEmDynfZLzlU4Cos0IDImmKfMOy7G5M0Trour44afjMnfGBTnY4nnhm+8vCu+r83TjjCUI4xI8cOddx4awwlzajtJHpijYEIdcMAkZu+YrdB+ycqF9s4HQpEUf+rR2n3OTJhyxbfseLqp4nlgxBeqXHHdoqfv/qX/RZFwcwtKJ3c+keK3WMrFlMs/kUvcsuPm3EQn3wLlwn+/TTB4KhIxUSQcXUzzo2KR1VgXe/EA/+8TLpgfK2FVia9berR89+CdLcaPFvlyXIhPjAgm9Dq5iRn8lbc/tHhszURyy0k6LwLINwgnH65ZrUje7S8Q8Y5NxKR8npfwHuj39TXxLEEn/84zaV35u0NJusXHnRvgcbx726e/P0DUg4fvWvnIpsr+V38nIslvcWLu/alklcuWdHC6Q1DN6EMIjOC7gqMGf+sxfreDH916WbZyEiBnlWuXu6AhJTpl8MLvWyAVQH7/6iTcDD6qxZlsPpRQnfdv80u5snX+gqqYSK3lx/ut1v5+a++NHmpOIhEOypLSIHqUI4GSSZd9DAulEtRZ5OT1u3evLwgJbScpl6fJ/EatXVy4hcnEUh683KVFoWcvkcUWyW3yFteG/7NMKem8U5JqZWtwiuZCI4we9hcTcxQuiCxKJFQxa/DQZyYsluCPWKJO4JGdus3XiclOyJNLGVxy/zgyw8PEIwYejD3hNlOUC7jksjSYnp7d9vtnpw8HVpJeATZ/7JN0m2meC5ABNgwQcJJ7cRHZk/ZCY2w8PPJzHvL45Hsavwc+Dq9K9NchrMaDB8VCHKzCBALo6aYJMDx22vlrNCSrdrZpBj5K5PhoOSYYN6c81FQeIOM6eky7pFyNua0YB5rJHPS5uoYmkslBOdG51uAxll1ly6pF/HCuhqUY5JaE8CMPhIhJVMLVrJx0e3x8fNh/8XAwiERGK3d0fnnGsspNT30BMJ/+oLwYuCXrPOhROt453O3fnp1BurK7wwKgyBsNBKjXaW2RimT1ocIqiQkzhox4A7P8FcBcdy6tBSo16TBYd5bLP8KLK8ASHz7kwFYi/3G6hmGC83nHUQIqO6eDcoQLBO7LCV+e6ZeAVWCpf3lEufi/heNoN4VCYRYx6T8MoNit0Zgh1ayX3N5ZfhLUH012X0yPMOzVIA6Wzw85i3y/CuaXJodnDwdHULsDIU5S3CXgO9kJBPaX0/Pcwc6GJ7QKSmHEmOykmQgFzwFQEITffodbMmuE4P1dO5y23bbXNzxwcP4fTARSLchqZW2fJJBRt9ex189k3USIY6rKsLGu0UOsmYhAZqpsykZ77SJZO5FRwZQV01nP8HOftGbVIpWhWrhfx/jCXNxIJhJ2SQoywuORJ+n5rDlaaqJPwp9BONEEIoL/1es1N+1ghOef3G8AJ9cejeqhZ58n4jgONpzRqM2mbM91Zg2nDg8FEr4SGIBg20E7eO54g+LPnShWWop61TU0VZMrdrDuIRGBNJVeVxCqsqYZHZhGuyJj56oT0iazTs+AH7tNgS4LviJXvLE6itxrs8f3ck/Zpq91zJ5yzx6OlV4Iub68VJ2qpiqmCW6kYHrDzBPZHprqfWNoKLJZ6JKRoikKdnZtv3O9UdBwANkYNhz2BPqoOBg0YfBhkw3lqmZhRJsN1XsIRAqm4sFQtBWINIayqWqmoiqy1nMSJLJdME1Yb1mGHmrV1TQFlt+UCx2vb9vQgJdhavBw6L3UNWTqlUA48LbWYPQUU3HZJzQ0ueATGWoIFddCMZfOi+CThqpSHbXbTVhyf+niRHozx5mB79cUrWo79QZ8rOHLrzEsuPd1u16BhxqTw3bwPvbE+YFx3cPDKnsnIEItjmLWA/Lbq/iL7WqbfVdlreGxmyciGy5dqXugBI8BDnDyAoFA7EqTrXPXmx5EPA24oxW1QQqKXBgjkYoqq7MYER9V+OnKWQsNAnX24UlEaBt6GEBkRJsdNVg+IRihGb7lgqphz/uC4nYV+tTpGYqnu3NEBPrueOhRX5EFwAZzM22muItEOrTVhg4G0x1Y3UI18FteHBkXPJnSAehMO1qh2ixQAiM1dOkLEsE1UhjzVYjgX7teHz9JRHicCKXhtEf1qhoQGbGmDZ5uVocBII7DOKhiCURgQDeua78PZ1xx0S8tSQS+tJsNHMHwHBSO6YJp2GSsmrIjuAoYOUwen5BEIhVP6iuhKRc0tVBQlyQCyl8xVA0GUEMivmVXNK2Cdqy4xIYfdvy5zxGh/sxwVzMQTEQhEjW2x6PlVAucllswDbPTHI+qWkhkht5A6CmgTdA2NXs2DDVr3tjRzZirhELC4pGsuDQTtRVzKSIdMPEO1ZnA2AFOT1E7I9Asm7YLYwxHdgIRkKjrhc+VAEOicweDfdprPUJEQJVRZIeOECGCPQwXNKtDIwh8b2haI/qpoWqBK8eMQHiiEvjfECCIqcztjbUlJEKDi9GjySLM1wyJQA4FYKFwxtrhqodEaPTRnqF+hxRCq4CXt6vaMqqFEoEIcA8d65AJmuGUbJkmgnRAB9um0o4ToUmAqVVnY4rZsiIBhwE5o9ZrNOSh0jPNgAiEgYAItH0ioEYBkSBvUmVFcxtdZSjLIRGMhT4v2p5bdlRoT7UwGTM18HmAYW8F3WoMYa3A/5qj5lAxfCJDZdjxiPhtIAI1h5edg68besZeh8QZgoha6LYhfQyJjCHzH/pFx9AwhpGA14XXPdXqqVjKUMB6LE0EfEa1ZximW7WJ3XC7NnvYdLuuHybuaZs62i6gTYlU4eG216HdkSHB6N6DLbhuJRgYeze8COg0ul034mAr8DrzxULH7QZwOytIBLOLeluI1uNkfi8hbAtJGxA4Qr1u05aQWMovthcHimBpHiTYVlhhLfzAtmooWBkrrsSzjJAhQ4YMGTJkyJAhQ4YMGTJkyJAhQ4YMGTJkyPB8+C/s2DyCKfoxUQAAAABJRU5ErkJggg=="
              width="50"
              height="50"
              className="d-inline-block align-top headerimg"
              style={{ borderRadius: "100%" }}
            />
            <h2>Almawiz HelpLine</h2>
          </div>
          <div className="status">
            <FontAwesomeIcon
              className="online"
              icon={faCircle}
            ></FontAwesomeIcon>
            <h4>Online</h4>
          </div>
        </Navbar>
      </div>
      <div className="panel">
        <form className="send-msg">
          <div id="chat-message-list">
            {conversation.map((message) =>
              message.sender === null ? (
                <div className="message-row admin-message" ref={setRef}>
                  <div className="message-content">
                    <div className="message-text">{message.message}</div>
                    <div className="message-time-admin">{}</div>
                  </div>
                </div>
              ) : message.sender === undefined ? (
                <div className="message-row you-message" ref={setRef}>
                  <div className="message-content">
                    <div className="message-text">{message.message}</div>
                    <div className="message-time">{message.time}</div>
                  </div>
                </div>
              ) : message.sender.type === "contact" ? (
                <div className="message-row you-message" ref={setRef}>
                  <div className="message-content">
                    <div className="message-text">{message.message}</div>
                    <div className="message-time">
                      {message.createdAt.substring(10, 20)}
                    </div>
                  </div>
                </div>
              ) : message.sender.type === "user" ? (
                <div className="message-row other-message" ref={setRef}>
                  <div className="message-content">
                    <div className="message-text">{message.message}</div>
                    <div className="message-time-sender">
                      {message.createdAt.substring(10, 20)}
                    </div>
                  </div>
                </div>
              ) : (
                ""
              )
            )}
          </div>
          <div className="send-bar">
            <Form.Group>
              <input
                type="text"
                ref={messageRef}
                // value={sentMessage}
                className="send-messages"
                // onChange={(e) => setsentMessage(e.target.value)}
                placeholder="Type a message"
              />
              <Button
                onClick={updateConversation}
                variant="success"
                type="submit"
                className="send-btn"
              >
                <FontAwesomeIcon
                  className="sendicon"
                  icon={faPaperPlane}
                ></FontAwesomeIcon>
              </Button>
            </Form.Group>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ChatBox;
