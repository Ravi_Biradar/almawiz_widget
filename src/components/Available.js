import React from 'react'
import './Widget.css'
import loading from '../components/assets/loading.gif'
function Availability() {
    return (
        <div className="card">
        <img className="loading" src={loading} alt="no" />
            <div className="message">
            
            Checking Agents availability
            </div>
        </div>
    )
}

export default Availability