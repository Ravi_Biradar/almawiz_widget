import React, { useState } from 'react'
import './Widget.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faComments, faTimes, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import './Home.css'
function Widget() {
    const [toggle,setToggle] = useState(true)
    const handleToggle =() =>{
setToggle(false)
    }
    const closeChat=()=>{
setToggle(true)

    }
    return (
        <div >
        <div className="home">
           { toggle?<button className="widget" onClick={handleToggle}><FontAwesomeIcon className="icon" icon={faComments}/ ></button>:<button className="widget"  onClick={closeChat}><FontAwesomeIcon className="icon" icon={faTimesCircle}/ ></button>}
           </div>
        </div>
    )
}

export default Widget
